
# Resume
[![Build Status](https://gitlab.com/petrosichor/resume/badges/master/build.svg)](https://gitlab.com/petrosichor/resume)

### *A responsive resume/cv site generator for GitLab Pages. Powered by Mustache templates engine & GitLab CI.*

Now your resume is public on [https://petrosichor.gitlab.io/resume/](https://petrosichor.gitlab.io/resume/)

## Work locally

To work locally do a **npm install** and **npm run build** to generate the html files on the **public** folder.

## Customize your resume

To add your style just modify the style.css file in the public folder or the mustache template file to change the layout.


## Contribution

Just fork this repo and make a merge request, or drop me a mail at [hola@luisfuentes.me](mailto:hola@luisfuentes.me) 👀

## License©️

Default theme designed by [Franklin Schamhart](https://dribbble.com/shots/1887983-Resume).

Resume is licensed under the MIT Open Source license. For more information, see the LICENSE file in this repository.
